# Error Tracking Ruby example

## Testing

```bash
cp .env.sample .env
# Set ERROR_TRACKING_DSN

docker build -t sentry-ruby .
docker run --rm -it --env-file .env sentry-ruby

# Verify error showing up on the configured (ERROR_TRACKING_DSN) project
```
