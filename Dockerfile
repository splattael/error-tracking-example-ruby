FROM ruby

# ENV ERROR_TRACKING_DSN=https://glet_203exxxxh3@gitlab.com/api/v4/error_tracking/collector/19618667

COPY Gemfile* ./
RUN bundle install

COPY test.rb .

CMD [ "ruby", "test.rb" ]
